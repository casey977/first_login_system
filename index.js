const express = require('express');

const app = express();

const address = '127.0.0.1';
const port = 4567;
const JWT_SECRET = 'kngoawegrhgerwh43h543427';
const HARDCODED_USERNAME = 'admin';
const HARDCODED_BAD_PASSWORD = '12345'

app.use(express.json());

app.get('/login', login_func);

app.listen(port, listen_func);

function listen_func()
	{
		console.log(`Server with Express running at ${address}:${port}`);
	}

function login_func(req, res)
	{
		const {username, password} = req.body;

		if(username === HARDCODED_USERNAME && password === HARDCODED_BAD_PASSWORD)
			{
				return res.json({
					'token': jsonwebtoken.sign({'user': HARDCODED_USERNAME}, JWT_SECRET)
				});
			}

		return res.status(401).json({'message': 'You need to log-in first.'});
	}